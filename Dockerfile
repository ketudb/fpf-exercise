FROM tiangolo/uwsgi-nginx-flask:python3.7
MAINTAINER Keturah Dola-Borg (keturah@mania.systems)

COPY ./flask-gh-pr-hook /app
RUN mkdir /app/data

EXPOSE 80